function onFormSubmit(){
insertNewRecord()
}

var rowCount = 0;

function insertNewRecord(){

    var vid = document.getElementById("id").value;
    var vname = document.getElementById("name").value;
    var vemail = document.getElementById("email").value;

    //clear input data from form on click submit
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";

    var table = document.getElementById("record");
    rowCount++;
    var row = table.insertRow(rowCount);
    //insertRow() will do the same as insertRow(rowCount)
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);

    cell1.textContent = vid;
    cell2.textContent = vname;
    cell3.textContent = vemail;
    //cell4.innerHTML = '<button id="delete" class="btn btn-sm btn-danger">Delete</button>';
    cell4.innerHTML = '<a class="btn btn-sm btn-danger" onClick="Delete(this)">Delete</a>';
    //cell5.innerHTML = '<button id="edit" class="btn btn-sm btn-success">Edit</button>';
    cell5.innerHTML = '<a class="btn btn-sm btn-success" onClick="Edit(this)">Edit</a>';
}

function Delete(arg){
    //alert("Are you sure you want to remove the data?");
    var table = document.getElementById("record");
    var rowIndex = arg.parentElement.parentElement.rowIndex;
    table.deleteRow(rowIndex);
    rowCount--;
}

function Edit(arg){
    var table = document.getElementById("record");
    var rowIndex = arg.parentElement.parentElement.rowIndex;

    console.log("this is the row index",rowIndex);

    var id = table.rows[rowIndex].cells[0].innerHTML;
    var name = table.rows[rowIndex].cells[1].innerHTML;
    var email = table.rows[rowIndex].cells[2].innerHTML;

    console.log(id,name,email);

    table.deleteRow(rowIndex);
    rowCount--;
    
    document.getElementById("id").value = id;
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;

}
